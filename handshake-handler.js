var inherits = require('util').inherits;
var EventEmitter = require('events').EventEmitter;

var HandshakeHandler = function () {
    this.bufferSize = 40;
    this.handshakeOngoing = false;
    this.inputData = [];
    for (var i = 0; i < this.bufferSize; i++) {
        this.inputData.push(0);
    }
    this.threshold = 35; //sensor values lower than or equal to this is ignored
    this.minValuesThatCountAsHandshake = 2;
};

inherits(HandshakeHandler, EventEmitter);

HandshakeHandler.prototype.resetInput = function () {
    this.inputData = [];
    for (var i = 0; i < this.bufferSize; i++) {
        this.inputData.push(0);
    }
    this.emit('inputReset');
};

HandshakeHandler.prototype.addInput = function () {
    for (var i = 0; i < arguments.length; i++) {
        this.inputData.shift();
        this.inputData.push(arguments[i]);
    }
    if (!this.handshakeOngoing) {
        this.checkIfHandshakeHasStarted();
    } else {
        this.checkIfHandshakeHasEnded(arguments.length);
    }
};

HandshakeHandler.prototype.checkIfHandshakeHasStarted = function () {
    var validCount = 0;
    var lastIdx = this.inputData.length - 1;
    for (var j = lastIdx; j > lastIdx - this.minValuesThatCountAsHandshake; j--) {
        var val = this.inputData[j];
        if (val > this.threshold) {
            validCount += 1;
        }
    }
    if (validCount == this.minValuesThatCountAsHandshake) {
        this.emit('handshakeStarted');
        this.handshakeOngoing = true;
        //console.log("A handshake is happening");
    }
};

HandshakeHandler.prototype.checkIfHandshakeHasEnded = function (valuesToCheck) {
    var lastIdx = this.inputData.length - 1;
    for (var j = lastIdx; j > lastIdx - valuesToCheck; j--) {
        var val = this.inputData[j];
        if (val <= this.threshold) {
            this.emit('handshakeEnded');
            this.handshakeOngoing = false;
        }
    }
};


HandshakeHandler.prototype.findIndexOfNextValidInput = function (idx) {
    var found = -1;
    for (var i = idx; i < this.inputData.length; i++) {
        var val = this.inputData[i];
        if (val >= this.threshold) {
            return i;
        }
    }
    return found;
};

//returns (potentially) multiple arrays of contiguos inputs as an array of arrays of integer values that are larger than
//the threshold in the input buffer
HandshakeHandler.prototype.getContiguousValidInputs = function () {

    var res = [];
    var nextValid;
    var indexToCheck = this.findIndexOfNextValidInput(0);

    while ((nextValid = this.findIndexOfNextValidInput(indexToCheck)) != -1) {
        //console.log("Checking from " + indexToCheck + ", nextValid = " + nextValid);
        var contiguousValidCount = 0;
        var contiguousValues = [];
        for (var i = nextValid; i < this.inputData.length; i++) {
            var val = this.inputData[i];
            if (val >= this.threshold) {
                contiguousValues.push(val);
                //console.log("Added val " + val);
                contiguousValidCount++;
            } else {
                //console.log("no more contiguous values");
                break;
            }
        }
        res.push(contiguousValues);
        if (indexToCheck < this.inputData.length) {
            indexToCheck = nextValid + contiguousValidCount;
        } else break;
    }
    return res;
};

module.exports = new HandshakeHandler();

