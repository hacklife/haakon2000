'use strict';

/**
 * Unit tests for Håkon 2000's handshake handler
 */
var handshakeHandler = require('../handshake-handler');
var handshakeAnalyzer = require('../handshake-analyzer');

var chai = require('chai'),
    expect = chai.expect,
    should = chai.should();


describe('Håkon 2000 handshake handler', function () {

    it('should have a buffer of 40 input data values', function () {
        expect(handshakeHandler.inputData.length).to.eql(40);
    });

    it('should have a zeroed-out buffer on reset', function () {
        handshakeHandler.resetInput();
        var data = handshakeHandler.inputData;
        var cumulative = 0;
        for (var i = 0; i < data.length; i++) {
            var x = data[i];
            cumulative += x;
        }
        expect(cumulative).to.eql(0);
    });

    it('should return an array of arrays with contiguous input data from the input buffer', function () {
        handshakeHandler.resetInput();
        handshakeHandler.addInput(200, 200, 0, 0, 0, 180, 178, 0, 0, 133, 133, 133, 133, 133, 133, 133, 133, 29, 0, 230);
        var result = handshakeHandler.getContiguousValidInputs();
        expect(result).to.be.eql([[200, 200], [180, 178], [133, 133, 133, 133, 133, 133, 133, 133], [230]]);
    });

    it('should find the index of the next valid input in the buffer', function () {
        handshakeHandler.resetInput();
        handshakeHandler.addInput(200);
        expect(handshakeHandler.findIndexOfNextValidInput(0)).to.eql(39);
        handshakeHandler.addInput(200);
        expect(handshakeHandler.findIndexOfNextValidInput(0)).to.eql(38);
        handshakeHandler.addInput(0, 0, 0, 0, 200);
        expect(handshakeHandler.findIndexOfNextValidInput(35)).to.eql(39);
    });

    it('should fire an event when input is reset', function (done) {
        handshakeHandler.on('inputReset', function () {
            done();
        });
        handshakeHandler.resetInput();
        handshakeHandler.removeAllListeners();
    });

    it('should fire a handshakeStarted event when detecting more than ' +
        '[minValuesThatCountAsHandshake] cycles of valid pressure on sensor', function (done) {
        handshakeHandler.on('handshakeStarted', function () {
            done();
        });
        handshakeHandler.resetInput();
        handshakeHandler.addInput(200, 200, 200);
        handshakeHandler.removeAllListeners();

    });

    it('should fire a handshakeEnded event when a handshake is ongoing and then ends', function (done) {
        handshakeHandler.resetInput();
        handshakeHandler.on('handshakeEnded', function () {
            done();
        });
        handshakeHandler.addInput(200, 200, 200);
        handshakeHandler.addInput(0);

    });


});
