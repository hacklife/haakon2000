# Haakon 2000

## Forberedelser:

* Installer node 6.7+ fra nodejs.org
* Kjør en "npm install" for å få installert dependencies
* Installer webserver globalt med "npm install http-server -g"

## Kjør

* Start sensor server med "npm run sensors" ( eller "npm run nosensors" hvis du ikke har tilgang til sensorene")
* Start webserver med "npm run webserver"

* Åpne https://localhost:9998/ - MÅ KJØRES I CHROME - https kreves pga at vi skal hente inn video fra webcam

## Unit tester

* Kjør unit-tester med "npm test"