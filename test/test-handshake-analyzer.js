var handshakeAnalyzer = require('../handshake-analyzer');

var chai = require('chai'),
    expect = chai.expect,
    should = chai.should();


describe('Håkon 2000 handshake analyzer', function () {


    it('should count as a good pressure handshake when average is 200', function () {
        expect(handshakeAnalyzer.isHandshakePressureOk([200, 200, 200])).to.eql(true);
    });

    it('should count as a bad pressuer handshake when average is above 214', function () {
        expect(handshakeAnalyzer.isHandshakeTooHard([215, 215, 215, 215, 215])).to.eql(true);
    });

    it('should count as a too soft handshake when average is below 120', function () {
        expect(handshakeAnalyzer.isHandshakeTooSoft([119, 119, 119, 119, 119])).to.eql(true);
    });

    it('should count as a bad handshake if handshake is too long', function() {
        expect(handshakeAnalyzer.isLengthTooLong([123, 123, 123, 123, 123, 123, 123, 123, 123, 123, 123, 123, 123])).to.eql(true);
    });

    it('should count as a bad handshake if handshake is too short', function() {
        expect(handshakeAnalyzer.isLengthTooShort([123, 123, 123, 123])).to.eql(true);
    });

    it('should count as a good handshake if handshake is just right', function() {
        expect(handshakeAnalyzer.isLengthOk([123, 123, 123, 123, 123])).to.eql(true);
    });

    it('should find the average from the given inputs', function() {
        expect(handshakeAnalyzer.getAverageFrom([100,200,100,200])).to.eql(150);
    });
});