var HandshakeAnalyzer = function () {
    this.tooHardHandshakeLimit = 214;
    this.tooSoftHandshakeLimit = 120;
    this.tooLongHandshakeLimit = 12;
    this.tooShortHandshakeLimit = 4;
};

HandshakeAnalyzer.prototype.getAverageFrom = function(handshakePressures) {
    var average = 0.0;
    var cumulative = 0;
    for (var i = 0; i < handshakePressures.length; i++) {
        cumulative += handshakePressures[i];
    }
    average = cumulative / handshakePressures.length;
    return average;
};

HandshakeAnalyzer.prototype.isHandshakePressureOk = function (handshakePressures) {
    var average = this.getAverageFrom(handshakePressures);
    return !(this.isHandshakeTooHard(average) || this.isHandshakeTooSoft(average));

};

HandshakeAnalyzer.prototype.isHandshakeTooSoft = function (handshakePressures) {
    return this.getAverageFrom(handshakePressures) <= this.tooSoftHandshakeLimit;
};

HandshakeAnalyzer.prototype.isHandshakeTooHard = function (handshakePressures) {
    return this.getAverageFrom(handshakePressures) >= this.tooHardHandshakeLimit;
};

HandshakeAnalyzer.prototype.isLengthTooLong = function (handshakePressures) {
    return handshakePressures.length > this.tooLongHandshakeLimit;
};

HandshakeAnalyzer.prototype.isLengthTooShort = function (handshakePressures) {
    return handshakePressures.length <= this.tooShortHandshakeLimit;
};
HandshakeAnalyzer.prototype.isLengthOk = function (handshakePressures) {
    return !this.isLengthTooLong(handshakePressures) && !this.isLengthTooShort(handshakePressures);
};

module.exports = new HandshakeAnalyzer();