document.addEventListener("DOMContentLoaded", function (event) {

    var mainCanvas = document.getElementById('rotated');
    var overlay = document.getElementById('overlay');
    var video = document.getElementById('input');
    var vendorURL = window.URL || window.webKitURL;
    var mainContext = mainCanvas.getContext("2d");
    var overlayContext = overlay.getContext("2d");
    var mainFont = "16px terminator";
    var smallFont = "11px Consolas";
    var objects = new tracking.ObjectTracker(['face']);
    var currentRect = {};
    var H_SCALE = 4.16666;
    var V_SCALE = 3.75;
    var DOTS_MAX_DELTA = 5;
    var BARS_MAX_DELTA = 6;
    var detectorRotation = 0;
    var lastHandshakeId = -99;
    var currentHandshake = "not detected";
    var currentResult = "not detected";
    var roles = ["coffee fetcher?", "developer?", "project lead?", "test lead?", "designer?"];

    var bufferCanvas = document.createElement('canvas');
    bufferCanvas.width = 800;
    bufferCanvas.height = 800;
    var bufferContext = bufferCanvas.getContext('2d');

    var subCanvas = document.createElement('canvas');
    subCanvas.width = 192;
    subCanvas.height = 160;
    var subContext = subCanvas.getContext('2d');

    var currentRole = getRandomPotentialRole();
    var currentHandshakeId = -1;
    var currentHandshakeResultReported = true;
    var voices;
    var dots = [];
    var bars = [];

    var Dot = function () {
        this.x = Math.round(Math.random() * 100);
        this.y = Math.round(Math.random() * 100);
        this.xDelta = getRandomPositiveOrNegativeDelta(5);
        this.yDelta = getRandomPositiveOrNegativeDelta(5);
    };

    var Bar = function () {
        this.val = Math.round(Math.random() * 100);
        this.valDelta = getRandomPositiveOrNegativeDelta(5);
    };

    for (var i = 0; i < 16; i++) {
        dots.push(new Dot());
    }

    for (var i = 0; i < 31; i++) {
        bars.push(new Bar());
    }
    setInterval(function () {
        currentRole = getRandomPotentialRole();
    }, 1000);

    // Chrome loads voices in the background ->
    // this method runs asynchronously and potentially more than once
    window.speechSynthesis.onvoiceschanged = function (e) {
        voices = loadVoices();
        console.log(voices);
    };

    //testing the speech synthesis
    setTimeout(function () {
        /*say("Vell comin till carry error dog in! yai come err frah bouvet ee behr genn!");
        say("Come heat, come heat! Day err inn gun ting aw vuhreh red for");
        say("Ow, ow ow! Day vare litt av eight hawned trick!");
        say("Eekeh ring, ohs, vee ring err day!");
        say("Day vare vell dig hug gelly aw moetay day");
        say("Hope er Vee see ess say nereh?");
        say("Haa en reek teeg goo dog!");
        */
    }, 2000);


    function say(text) {
        var utterance = new SpeechSynthesisUtterance(text);
        utterance.voice = voices[4];
        utterance.pitch = 1;
        utterance.rate = 0.9;
        window.speechSynthesis.speak(utterance);
    }

    function loadVoices() {
        var voiceAlternatives = window.speechSynthesis.getVoices();
        return voiceAlternatives;
    }

    var sock = new SockJS('https://localhost:9999/haakon2000');
    sock.onopen = function () {
        console.log('Socket connection opened');
        sendHello();
    };


    sock.onmessage = function (e) {
        console.log('New state from server: ', e.data);
        var data = JSON.parse(e.data);
        currentHandshake = data.handshake;
        currentHandshakeId = data.handshakeId;
        if(currentHandshakeId != lastHandshakeId){
            //a new handshake has started
            lastHandshakeId = currentHandshakeId;
            currentHandshakeResultReported = false;
        } else {
            if(data.handshakeResult != undefined && !currentHandshakeResultReported){
                if(data.handshakeResult == 'tooHard'){
                    say("Ow, ow ow! That was way too hard! Don't call us, we'll call you.");
                    currentHandshakeResultReported = true;
                } else if (data.handshakeResult == 'tooSoft'){
                    say("That was a really wimpy handshake");
                    currentHandshakeResultReported = true;
                }else if (data.handshakeResult == 'tooLong'){
                    say("Oh my. That was just embarrasingly long.");
                    currentHandshakeResultReported = true;
                }else if (data.handshakeResult == 'tooShort'){
                    say("Oh! That was a really short handshake!");
                    currentHandshakeResultReported = true;
                } else if (data.handshakeResult == 'justRight'){
                    say("Welcome to Bouvet! You have a very pleasant handshake. Hope to see you later!");
                    currentHandshakeResultReported = true;
                }
            }
        }
        currentResult = data.details;
    };

    sock.onclose = function () {
        console.log('closing socket');
    };

    function sendHello() {
        sock.send('Client sez HELLO');
    }

    objects.on('track', function (event) {
        if (event.data.length === 0) {
            //no face tracking
        } else {
            var rect = event.data[0];
            currentRect.x = rect.x;
            currentRect.y = rect.y;
            currentRect.width = rect.width;
            currentRect.height = rect.height;
        }
    });

    navigator.getUserMedia = navigator.getUserMedia ||
        navigator.webkitGetUserMedia ||
        navigator.mozGetUserMedia ||
        navigator.msGetUserMedia;

    //get camera video stream
    navigator.getUserMedia({
        video: true,
        audio: false
    }, function (stream) {
        video.src = vendorURL.createObjectURL(stream);
        video.play();
    }, function (err) {
        console.log(err);
    });

    video.addEventListener('play', function () {
        drawEverything(this, 800, 600);
    }, false);

    function updateFaceDetector() {
        detectorRotation += 4;
        detectorRotation %= 360;
    }

    function drawEverything(video, width, height) {

        bufferContext.save();
        bufferContext.translate(0, 600);
        // rotate the canvas to the specified degrees
        bufferContext.rotate(-90*Math.PI/180);
        // draw the image
        // since the context is rotated, the image will be rotated also
        bufferContext.drawImage(video,0,0);
        bufferContext.restore();

        subContext.drawImage(bufferCanvas, 0, 250, 450, 350, 0,0,192,160);
        tracking.track(subCanvas, objects);
        mainContext.drawImage(subCanvas, 0, 0, 192, 160, 0,0,800,600);

        overlayContext.clearRect(0, 0, 800, 600);
        drawFaceDetector(currentRect.x, currentRect.y, currentRect.width, currentRect.height);
        drawBinaryPattern();
        updateDots();
        drawDots(680, 150);
        drawBackgrounds();
        drawLabels();
        updateBars();
        drawBars(780, 280);
        setTimeout(function () {
            updateFaceDetector();
            drawEverything(video, width, height)
        }, 10);
    }


    function drawFaceDetector(x, y, w, h) {
        var sx = x * H_SCALE,
            sy = y * V_SCALE,
            sw = w * H_SCALE,
            sh = h * V_SCALE,
            centerX = (sx + (sw / 2)),
            centerY = (sy + (sh / 2));
        overlayContext.strokeStyle = "rgba(255,255,255,0.4)";
        overlayContext.lineWidth = 10;
        //overlayContext.rect(x*H_SCALE, y*V_SCALE, w*H_SCALE, h*V_SCALE);
        overlayContext.save();
        overlayContext.translate(centerX, centerY);
        overlayContext.rotate(degreesToRadians(detectorRotation));
        overlayContext.beginPath();
        overlayContext.arc(0, 0, sw / 1.5, degreesToRadians(0), degreesToRadians(80));
        overlayContext.stroke();
        overlayContext.beginPath();
        overlayContext.arc(0, 0, sw / 1.5, degreesToRadians(90), degreesToRadians(170));
        overlayContext.stroke();
        overlayContext.beginPath();
        overlayContext.arc(0, 0, sw / 1.5, degreesToRadians(180), degreesToRadians(260));
        overlayContext.stroke();
        overlayContext.beginPath();
        overlayContext.arc(0, 0, sw / 1.5, degreesToRadians(270), degreesToRadians(350));
        overlayContext.stroke();
        overlayContext.restore();
    }

    function drawBackgrounds(){
        overlayContext.fillStyle = "rgba(0,0,0,0.5)";
        overlayContext.fillRect(15,15, 650, 30);
        overlayContext.fillRect(15,495, 650, 95);
    }


    function drawLabels() {
        drawText("scan mode: first impression analysis", 20, 35, "#eee");
        drawText("handshake: ", 20, 520, "#ccc");

        drawText(currentHandshake, 280, 520, "#f00");
        drawText("result:", 20, 550, "#ccc");
        drawText(currentResult, 280, 550, "#f00");
        drawText("potential role:", 20, 580, "#ccc");
        drawText(currentRole, 280, 580, "#fff");
    }

    function drawText(text, x, y, fillstyle) {
        overlayContext.font = mainFont;
        overlayContext.fillStyle = fillstyle ? fillstyle : "#fff";
        overlayContext.fillText(text, x, y);
        overlayContext.strokeStyle = "#000";
        overlayContext.lineWidth = 1;
        overlayContext.strokeText(text, x, y);
    }



    function drawBinaryPattern() {
        overlayContext.fillStyle = "rgba(0,0,255,0.4)";
        overlayContext.font = smallFont;
        for (var i = 0; i < 8; i++) {
            overlayContext.fillText(getRandomBinaryString(), 680, 30 + (i * 15));
        }
    }

    function getRandomBinaryString() {
        var num = Math.round((Math.random() * 65535)).toString(2);
        return integerToBinary(num, 16);
    }

    function integerToBinary(int, length) {
        var out = "";
        while (length--)
            out += (int >> length ) & 1;
        return out;
    }

    function degreesToRadians(degrees) {
        return degrees * (Math.PI / 180)
    }

    function updateDots() {
        for (var i = 0; i < dots.length; i++) {
            updateDot(dots[i]);
        }
    }

    function updateBars() {
        for (var i = 0; i < bars.length; i++) {
            updateBar(bars[i]);
        }
    }

    function drawDots(startX, startY) {
        overlayContext.strokeStyle = "rgba(255,255,255,0.4)";
        overlayContext.fillStyle = "rgba(255,0,0,0.4)";
        overlayContext.lineJoin = 'miter';
        overlayContext.lineWidth = 1;
        for (var i = 0; i < dots.length; i++) {
            var dot = dots[i];
            var nextDot = dots[(i + 1) % dots.length];
            overlayContext.beginPath();
            overlayContext.moveTo(startX + dot.x, startY + dot.y);
            overlayContext.lineTo(startX + nextDot.x, startY + nextDot.y);
            if (i != 0) {
                overlayContext.lineTo(startX + dots[0].x, startY + dots[0].y);
            }
            if (i != dots.length - 1) {
                overlayContext.moveTo(startX + dot.x, startY + dot.y);
                overlayContext.lineTo(startX + dots[dots.length - 1].x, startY + dots[dots.length - 1].y);
            }
            overlayContext.stroke();
            overlayContext.closePath();
            overlayContext.fillRect(startX + dot.x - 1, startY + dot.y - 1, 3, 3);
        }
    }

    function drawBars(startX, startY) {
        overlayContext.fillStyle = "rgba(0,0,255,0.4)";
        for (var i = 0; i < bars.length; i++) {
            var bar = bars[i];
            overlayContext.fillRect(startX - bar.val, startY + (i * 10), bar.val, 5);
        }
    }

    function updateBar(bar) {
        bar.val += bar.valDelta;
        if (bar.val <= 0) {
            bar.val = 0;
            bar.valDelta = getRandomPositiveOrNegativeDelta(BARS_MAX_DELTA);
        }
        if (bar.val >= 100) {
            bar.val = 100;
            bar.valDelta = getRandomPositiveOrNegativeDelta(BARS_MAX_DELTA);
        }
    }

    function updateDot(dot) {
        dot.x += dot.xDelta;
        dot.y += dot.yDelta;
        if (dot.x <= 0) {
            dot.x = 0;
            dot.xDelta = getRandomPositiveOrNegativeDelta(DOTS_MAX_DELTA);
        }
        if (dot.x >= 100) {
            dot.x = 100;
            dot.xDelta = getRandomPositiveOrNegativeDelta(DOTS_MAX_DELTA);
        }
        if (dot.y <= 0) {
            dot.y = 0;
            dot.yDelta = getRandomPositiveOrNegativeDelta(DOTS_MAX_DELTA);
        }
        if (dot.y >= 100) {
            dot.y = 100;
            dot.yDelta = getRandomPositiveOrNegativeDelta(DOTS_MAX_DELTA);
        }
    }

    function getRandomPositiveOrNegativeDelta(max) {
        var positive = Math.random() < .5;
        return positive ? Math.round(Math.random() * max)+1 : Math.round(Math.random() * -max)+1;
    }

    function getRandomPotentialRole() {
        return roles[Math.floor(Math.random() * roles.length)];
    }

});