var https = require('https');
var sockjs = require('sockjs');
var fs = require('fs');
var robot = require('johnny-five');
var handshakeHandler = require('./handshake-handler.js');
var handshakeAnalyzer = require('./handshake-Analyzer.js');
var greenLed, redLed;
var blinkCounter = 0;

/**
 * Haakon 2000 server
 *
 * */
var options = {
    key: fs.readFileSync('ssl/key.pem'),
    cert: fs.readFileSync('ssl/cert.pem')
};

//the object being transmitted to the browser every so often
var currentSensorState = {
	handshakeId : 0,
	handshakeResult: undefined,
    handshake: 0.0,
    details: undefined,
	humidity: 0.0,
	pressure: 0.0,
	tilt: 0,
	handshakeWasOk: undefined
};

//start with 'npm start noboard' if you don't have the Arduino connected
if(process.argv[2] !=  'noboard') {
    var board = new robot.Board();

	//can do nothing until board is ready
    board.on("ready", function () {
        console.log("Board ready!");

        //initalise pressure sensor
        var pressure = new robot.Sensor({
            pin: 'A0',
            freq: 200
        });
		
        //intialise gree led light
        greenLed = new robot.Led(5);
		greenLed.brightness(255);

        //intialise gree led light
        redLed = new robot.Led(3);
		redLed.brightness(255);
		
		//initialise tiltUp-sensor
		var tiltUp = new robot.Sensor.Digital(10);

		//initialise tiltDown-sensor
		var tiltDown = new robot.Sensor.Digital(11);	
		
		//initialise humiditysensor
		var humidity = new robot.Sensor({
		   pin: 'A2',
		   freq: 25
		});	
		
        // handle pressure
        pressure.scale([0, 255]).on('data', function () {
            var val = this.scaled;
            currentSensorState.pressure = Math.round(val);
			handshakeHandler.addInput(currentSensorState.pressure);
        });
		
		// handle tilt up
		tiltUp.on("change", function() {
			
			if (this.value) {
				currentSensorState.tilt = 1;
			}
			else {
				currentSensorState.tilt = 0;	
			}
			
			console.log("tiltUp changed! New value: " + this.value);
		});	
		
		// handle tilt down
		tiltDown.on("change", function() {
			
			if (this.value) {
				currentSensorState.tilt = -1;
			}
			else {
				currentSensorState.tilt = 0;	
			}
			
			console.log("tiltDown changed! New value: " + this.value);
		});	
		
		// handle humidity
		humidity.scale([0, 255]).on('data', function() {
			var val = 255 - this.scaled;			
			currentSensorState.humidity = Math.round(val);
			//console.log("Humidity: " + val);
		});
		resetData();
    });

}


function blinkGreen(){
	greenLed.blink(50);
	setTimeout(function () {
		greenLed.stop().off()
	}, 2500);
}

function blinkRed(){
	redLed.blink(50);
	setTimeout(function () {
		redLed.stop().off()
	}, 2500);
}

function resetData(){
	greenLed.on();
	redLed.off();
	currentSensorState.handshake = "None detected";
	currentSensorState.details = "N/a";
	currentSensorState.handshakeResult = undefined;
}

handshakeHandler.on('handshakeStarted', function () {

    console.log("Handshake started.");
	greenLed.on();

    //TODO: a handshake has started! Say something?
	currentSensorState.handshakeId +=1;
	currentSensorState.handshakeResult = undefined;
	currentSensorState.handshake = "Analyzing";
});
handshakeHandler.on('handshakeEnded', function () {
	greenLed.off();

	var validInputs = handshakeHandler.getContiguousValidInputs();
	var lastHandshakeData = validInputs[validInputs.length-1];
	console.log("Last handshake data: "+lastHandshakeData);
    console.log("Handshake ended.");
	var avg = Math.round(handshakeAnalyzer.getAverageFrom(lastHandshakeData));
	if(handshakeAnalyzer.isHandshakeTooHard(lastHandshakeData)){
		blinkRed();
		console.log("Too hard!");
		currentSensorState.details = "Too hard! ("+avg+")";
		currentSensorState.handshakeResult = "tooHard";
	} else if(handshakeAnalyzer.isHandshakeTooSoft(lastHandshakeData)) {
		blinkRed();
		currentSensorState.details = "Too soft! ("+avg+")";
		console.log("Too soft!");
		currentSensorState.handshakeResult = "tooSoft";
	} else if(handshakeAnalyzer.isLengthTooShort(lastHandshakeData)) {
		blinkRed();
		console.log("Too short!");
		currentSensorState.details = "Too short!";
		currentSensorState.handshakeResult = "tooShort";
	} else if(handshakeAnalyzer.isLengthTooLong(lastHandshakeData)) {
		blinkRed();
		console.log("Too long!");
		currentSensorState.details = "Too long!";
		currentSensorState.handshakeResult = "tooLong";
	} else{
		currentSensorState.handshakeResult = "justRight";
		currentSensorState.details = "Perfect! ("+avg+")";
		blinkGreen();
		console.log("Pressure OK");
	}

	setTimeout(resetData, 5000);
});


var sockjsServer = sockjs.createServer({sockjs_url: 'http://cdn.jsdelivr.net/sockjs/1.0.1/sockjs.min.js'});

sockjsServer.on('connection', function (conn) {

    //send current sensor state every 2 sec
    setInterval(function () {
        //update sensor data, then
        conn.write(JSON.stringify(currentSensorState));
    }, 200);

    //if we need data from client, it would happen here...
    conn.on('data', function (message) {
        console.log("Data from client: " + message);
    });

    conn.on('close', function () {
        console.log("Connection closed.")
    });
});

var httpsServer = https.createServer(options);
sockjsServer.installHandlers(httpsServer, {prefix: '/haakon2000'});
httpsServer.listen(9999, '0.0.0.0');
